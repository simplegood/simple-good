Long Island's first zero waste lifestyle shop! Visit us to find a large selection of sustainable and eco friendly home goods, organic and package free self care and sustainable apparel for infants to adults! 5% of profits donated to the Sierra Club.

Address: 35 Chandler Sq, Port Jefferson, NY 11777, USA

Phone: 631-509-6659

Website: https://simplegoodgifts.com
